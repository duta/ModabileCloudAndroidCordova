
  $(document)
    .ready(function() {
    	$(".dropdown")
    	  .dropdown({
		    on: 'hover'
		  })
    	;
      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {	
              $('.fixed.menu').transition('swing down in');
          },
          onBottomPassedReverse: function() {
        	  
            $('.fixed.menu').transition('horizontal flip out fest');
          }
        })
      ;
      // create sidebar and attach to menu open
      
      $('.toc')
        .on('click', function() {
          var
            transition = $(this).data('transition')
          ;
          $('.ui.sidebar')
            .not('.styled')
           // .sidebar({context:$('#body')})
            .sidebar('setting', {
              transition       : transition,
              mobileTransition : transition
            })
          ;
          $('.ui.sidebar').not('.styled').sidebar('toggle');
        })
    ;
    
      $('.ui.checkbox')
      .checkbox()
    ;
    });
  