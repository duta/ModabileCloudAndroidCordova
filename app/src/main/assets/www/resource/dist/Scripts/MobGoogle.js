﻿var map, layer;
var epsg4326 = new OpenLayers.Projection('EPSG:4326');
var epsg900913 = new OpenLayers.Projection('EPSG:900913');
$(document).ready(function () {
    initMap();
    initMapLayers(map);

    ChangeProjection(map, vector_layerGeoJson);
    map.addLayer(vector_layerGeoJson);

    ChangeProjection(map, ori_layerGeoJson);
    map.addLayer(ori_layerGeoJson);

    ChangeProjection(map, inb_layerGeoJson);
    map.addLayer(inb_layerGeoJson);

    ChangeProjection(map, linbe_layerGeoJson);
    map.addLayer(linbe_layerGeoJson);
});

function initMap() {
    map = new OpenLayers.Map('mapGeoJson', {theme:"resource/dist/theme/default/style-v2.css"});
    var layer = new OpenLayers.Layer.Google(
                "Google Earth", // the default
                {
                    numZoomLevels: 20,
                    format: "image/png",
                    projection: epsg900913,
                    displayProjection: epsg4326
                }
            );
    map.addLayer(layer);
    map.zoomToMaxExtent();
    map.setCenter(new OpenLayers.LonLat(lon, lat).transform(epsg4326, epsg900913), zoom);
}

function getBounds() {
    return map.calculateBounds().transform(epsg900913, epsg4326);
}

function getCursorLonLat(mouseEvent) {
    return map.getLonLatFromViewPortPx(mouseEvent.xy).transform(epsg900913, epsg4326);
}