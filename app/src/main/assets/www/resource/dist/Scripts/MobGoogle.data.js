var vector_layerGeoJson, ori_layerGeoJson, linbe_layerGeoJson;
var renderer = GetRenderer();
var featuresShipOnDisplay;
var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;

$(document).ready(function () {
    var token = getUrlParameter('token');
    var email = getUrlParameter('email');
    $.ajax({
        type: "GET",
        url: 'https://dummy-1-1-dot-modabile.appspot.com/vessel?op=getcustomerdata&token='+token,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            if (data != null)
                $(".user-label").html(data.company);
            else
                $(".user-label").html("");
        }
    });
    $("#lblUser").html(email);
    RequestAjaxShips(token);
});

function GetRenderer() {
    renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
    return renderer;
}

function ChangeProjection(map, layer) {
    layer.projection = map.displayProjection;
    layer.preFeatureInsert = function (feature) {
        feature.geometry.transform(epsg4326, epsg900913);
    };
}

function RequestAjaxShips(token) {
    token = JSInterface.getToken();
    try {
        $.ajax({
            type: "GET",
            url: 'https://dummy-1-1-dot-modabile.appspot.com/vessel?op=getall&token='+token,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                updateMapWithGeoJsonColl(data);
                setTimeout(function () {
                    RequestAjaxShips(token);
                }, 30000);
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function initMapLayers(mapGeoJson) {

    var contextShips = {
        getShipStyle: function (ft) {
            return GetShipImagePath(ft);
        },
        getShipWidth: function (ft) {
            return 30; //image width
        },
        getShipHeight: function (ft) {
            return 19; //image height
        },
        getShipLabel: function (ft) {
            var knot = (ft.attributes.SOG == undefined) ? 0 : (ft.attributes.SOG);
            return mapGeoJson.getScale() < 10000000 ? (ft.attributes.ShipName == null ? ft.attributes.MMSI : ft.attributes.ShipName) +
                "\n" + FormatTimestampReceiver(ft) + " / " + knot + " k" : "";
        }
    };

    var templateShip = {
        externalGraphic: "${getShipStyle}", // using context.getSize(feature)
        // Makes sure the background graphic is placed correctly relative
        // to the external graphic.
        backgroundXOffset: 0,
        backgroundYOffset: -7,
        //AAV:set size
        graphicWidth: "${getShipWidth}",
        graphicHeight: "${getShipHeight}",

        //show label and rotate image
        label: "${getShipLabel}",
        labelXOffset: 15,
        fontColor: "black",
        labelAlign: "l",
        labelOutlineColor: "beige",
        labelOutlineWidth: 3,
        //display: "${getShipVisibility}",

        // Set the z-indexes of both graphics to make sure the background
        // graphics stay in the background (shadows on top of markers looks
        // odd; let's not do that).
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX,
        pointRadius: 10
    };

    var styleShip = new OpenLayers.Style(templateShip, { context: contextShips });
    vector_layerGeoJson = new OpenLayers.Layer.Vector(
                    "ship_layer",
                    {
                        styleMap: new OpenLayers.StyleMap(styleShip),
                        isBaseLayer: false,
                        rendererOptions: { yOrdering: true },
                        renderers: renderer
                    });

    var templateOri = {
        styleMap: new OpenLayers.Style({
            externalGraphic: "resource/dist/Styles/static_ori.png",
            graphicWidth: 15,
            graphicHeight: 15,
            pointRadius: 0
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    ori_layerGeoJson = new OpenLayers.Layer.Vector("ori_layer", templateOri);

    var templateInb = {
        styleMap: new OpenLayers.Style({
            externalGraphic: "resource/dist/Styles/static_inb.png",
            graphicWidth: 15,
            graphicHeight: 15,
            pointRadius: 0
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    inb_layerGeoJson = new OpenLayers.Layer.Vector("inb_layer", templateInb);

    var templateLinbe = {
        styleMap: new OpenLayers.Style({
            strokeColor: "Green",
            strokeWidth: 2
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    linbe_layerGeoJson = new OpenLayers.Layer.Vector("linbe_layer", templateLinbe);
}

function updateMapWithGeoJsonColl(featColl) {
    featuresShipOnDisplay = {};
    //featColl = $.parseJSON(featColl);
    vector_layerGeoJson.removeFeatures(vector_layerGeoJson.features);
    ori_layerGeoJson.removeFeatures(ori_layerGeoJson.features);
    inb_layerGeoJson.removeFeatures(inb_layerGeoJson.features);
    linbe_layerGeoJson.removeFeatures(linbe_layerGeoJson.features);
    var geojson_format = new OpenLayers.Format.GeoJSON();
    
    console.log(featColl);
    console.log(featColl[0][0]);
    for (var i = 0; i < featColl[0].length; i++) {
        var currentShip = geojson_format.read(featColl[0][i]);
        console.log(currentShip);        
        if (currentShip.length > 0){
        	vector_layerGeoJson.addFeatures(currentShip[currentShip.length - 1]);
        	ori_layerGeoJson.addFeatures(currentShip[0]);
        	featuresShipOnDisplay[currentShip[0].attributes.MMSI] = currentShip;
        } 
        for (var j = 1; j < currentShip.length - 1; j++) {
            inb_layerGeoJson.addFeatures(currentShip[j]);
        }        
    }
    for (var i = 0; i < featColl[1].length; i++) {
        var coords = [];
        for (var j = 0; j < featColl[1][i].length; j++) {
            coords.push(new OpenLayers.Geometry.Point(featColl[1][i][j][0], featColl[1][i][j][1]))
        }
        var line = new OpenLayers.Geometry.LineString(coords);
        linbe_layerGeoJson.addFeatures(new OpenLayers.Feature.Vector(line));
    }
    WriteAllDetectedShips(vector_layerGeoJson.features);
}

function WriteAllDetectedShips(AllShipsOnDisplay) {
    $('.span-ship-count').html('0');
    var list = "";
    for (var i = 0; i < AllShipsOnDisplay.length; i++) {
        $('#ssc').html(parseInt($('#ssc').html()) + 1);
        list += '<div class="item item-detail" style="cursor:pointer" onclick="FindFeatureAndSelect(' + AllShipsOnDisplay[i].attributes.MMSI + ');ToggleMenu();">';
        list += '<img class="ui avatar image" src="resource/dist/asset/img/ship2.png">';
        list += '<div class="content">';
        list += '<div class="header">' + AllShipsOnDisplay[i].attributes.ShipName + '</div>';
        list += '<div class="headerSub"><i class="wifi icon"></i> ' + AllShipsOnDisplay[i].attributes.MMSI + ' <i class="wait icon"></i>' + FormatTimestampReceiver(AllShipsOnDisplay[i]) + '</div>';
        list += '</div>';
        list += '</div>';
    }
    $("#ship-table").html(list);
}

function FindFeaturesShip() {
    var textToSearch = text2.searchdata();
    var AllShipsOnDisplay = vector_layerGeoJson.features;
    if (textToSearch == "" || textToSearch.length == 1) return "";
    if (AllShipsOnDisplay == null) return "";

    var feature;
    var res = [];
    var jmlKapalterpilih = 0;
    var textHTMLAIS = '';
    var xyPos;
    for (var i = 0; i < AllShipsOnDisplay.length; i++) {
        feature = AllShipsOnDisplay[i];
        if (feature.attributes.MMSI == null) continue;
        xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
        if (feature.attributes.MMSI.toString().indexOf(textToSearch) != -1) {
            res.push([feature.attributes.MMSI, feature.attributes.MMSI, feature.attributes.ShipName]);
        }
    }

    if (jmlKapalterpilih > 0) {
        textHTMLAIS += '</div>';
        return textHTMLAIS;
    }
    else {
        for (var i = 0; i < AllShipsOnDisplay.length; i++) {
            feature = AllShipsOnDisplay[i];
            if (feature.attributes.ShipName == null) continue;
            xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
            if (feature.attributes.ShipName.toString().indexOf(textToSearch.toUpperCase()) != -1) {
                res.push([feature.attributes.MMSI, feature.attributes.MMSI, feature.attributes.ShipName]);
            }
        }
    }

    if (jmlKapalterpilih > 0) {
        textHTMLAIS += '</div>';
        return textHTMLAIS;
    }
    else {
        for (var i = 0; i < AllShipsOnDisplay.length; i++) {
            feature = AllShipsOnDisplay[i];
            if (feature.attributes.CallSign == null) continue;
            xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
            if (feature.attributes.CallSign.toString().replace(/ /g, '').toUpperCase().indexOf(textToSearch.replace(/ /g, '').toUpperCase()) != -1) {
                res.push([feature.attributes.MMSI, feature.attributes.MMSI, feature.attributes.ShipName]);
            }
        }
    }

    return res;
}

function GetShipImagePath(ft) {
    return "resource/dist/Styles/penumpang_static.png";
}

function FormatTimestampReceiver(feature) {
    return feature.attributes.TimeStampReceiver.toString().substring(6, 8) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(4, 6) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(0, 4) +
    ' ' + feature.attributes.TimeStampReceiver.toString().substring(8, 10) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(10, 12) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(12, 14);
}

function FindFeatureAndSelect(mmsi) {
    feat = vector_layerGeoJson.getFeaturesByAttribute("MMSI", mmsi);
    onFeatureGJSelect(feat[0]);
}

function FeatureIndexed(mmsi, index) {
    onFeatureGJSelect(featuresShipOnDisplay[mmsi][index]);
}

function onFeatureGJSelect(feature) {
    if (feature.geometry != null) map.moveTo(new OpenLayers.LonLat(feature.geometry.x, feature.geometry.y), 16, null);
    $('.imageShip').html('<img src="Uploaded/' + feature.attributes.MMSI + '.png" onerror="$(this).remove();" onload="$(this).parent(\'.imageShip\').css(\'background-image\',\'none\')" style="margin: 0 auto; " width="190" height="130" id="imgKapal" />');
    $('#detail-shipname').html(feature.attributes.ShipName);
    var shipIndex = featuresShipOnDisplay[feature.attributes.MMSI].indexOf(feature);
    $('#detail-next').prop('onclick', null).off('click');
    $('#detail-prev').prop('onclick', null).off('click');
    if (shipIndex != featuresShipOnDisplay[feature.attributes.MMSI].length - 1) {
        $('#detail-next').click(function () { FeatureIndexed(feature.attributes.MMSI, (shipIndex + 1)); });
        $('#detail-next').css('cursor', 'pointer');
        $('#detail-next').html('<i class="list icon"></i>Next');
    } else {
        $('#detail-next').css('cursor', 'default');
        $('#detail-next').html('');
    }
    if (shipIndex != 0) {
        $('#detail-prev').click(function () { FeatureIndexed(feature.attributes.MMSI, (shipIndex - 1)); });
        $('#detail-prev').css('cursor', 'pointer');
        $('#detail-prev').html('<i class="list icon"></i>Prev');
    } else {
        $('#detail-prev').css('cursor', 'default');
        $('#detail-prev').html('');
    }

    $('#detail-mmsi').html(feature.attributes.MMSI);
    $('#detail-callsign').html(feature.attributes.CallSign);
    $('#detail-loa').html((parseFloat(feature.attributes.ToBow) + parseFloat(feature.attributes.ToStern))+" m");
    $('#detail-wide').html((parseFloat(feature.attributes.ToPort) + parseFloat(feature.attributes.ToStarBoard)) + ' m');
    $('#detail-grt').html(feature.attributes.Grt);
    $('#detail-destination').html(feature.attributes.Destination);
    $('#detail-type').html(feature.attributes.ShipTypeDescription);
    $('#detail-sog').html(feature.attributes.SOG);
    $('#detail-cog').html(feature.attributes.COG);
    $('#detail-heading').html(feature.attributes.HDG);
    $('#detail-draught').html(feature.attributes.Draught);
    $('#detail-nav-status').html(feature.attributes.NavStatusDescription);
    var transformedLonLat = new OpenLayers.LonLat(feature.geometry.x, feature.geometry.y).transform(epsg900913, epsg4326);
    $('#detail-posisi').html((feature.geometry != null ? 'Lat ' + transformedLonLat.lat.toFixed(3) + ' Lon ' + transformedLonLat.lon.toFixed(3) : 'Lat - Lon -'));
    $('#detail-timestamp').html(FormatTimestampReceiver(feature));

    $("#viewKapal").hide();
    $("#viewDetailKapal").show();
    $("#viewMenu").hide();
    $("#viewRecorded").hide();
    if (!$('.detailView').hasClass('active')) $('.detailView').toggleClass('active');
    for (var i = 0; i < inb_layerGeoJson.features.length - 1; i++) {
        inb_layerGeoJson.features[i].style = {
            externalGraphic: "resource/dist/Styles/static_inb.png",
            graphicWidth: 15,
            graphicHeight: 15,
            pointRadius: 0
        };
    }
    if ((shipIndex > 0) && (shipIndex < featuresShipOnDisplay[feature.attributes.MMSI].length - 1)) {
        featuresShipOnDisplay[feature.attributes.MMSI][shipIndex].style = {
            externalGraphic: "resource/dist/Styles/static_inb.png",
            graphicWidth: 25,
            graphicHeight: 25,
            pointRadius: 0
        };
    }
    inb_layerGeoJson.redraw();
}