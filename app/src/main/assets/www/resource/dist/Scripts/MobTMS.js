﻿var map, tmsLayer, osmLayer;
function pad(pad, str, padLeft) {
    if (typeof str === 'undefined')
        return pad;
    if (padLeft) {
        return (pad + str).slice(-pad.length);
    } else {
        return (str + pad).substring(0, pad.length);
    }
};
var epsg4326 = new OpenLayers.Projection('EPSG:4326');
var epsg900913 = new OpenLayers.Projection('EPSG:900913');
$(document).ready(function () {
        $('<TileMapService version="1.0.0">'+
        		'<TileMaps>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/TX97" srs="EPSG:900913" title="Transas TX97 Day View" tileSize="256" profile="global-mercator"/>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/tx97-oldgen" srs="EPSG:900913" title="Transas TX97 Day View (OldGen)" tileSize="256" profile="global-mercator"/>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/tx97-night" srs="EPSG:900913" title="Transas TX97 Night View" tileSize="256" profile="global-mercator"/>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/tx97-transp" srs="EPSG:900913" title="Transas TX97 Transparent Shore" tileSize="256" profile="global-mercator"/>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/utt" srs="EPSG:900913" title="Transas UTT" tileSize="256" profile="global-mercator"/>'+
        		'<TileMap href="http://wms.transas.com/TMS/1.0.0/utt-demo-wor" srs="EPSG:900913" title="Transas UTT + World Ocean Reference" tileSize="256" profile="global-mercator"/>'+
        		'</TileMaps>'+
        		'</TileMapService>').find("TileMap").each(function (index, element) {
            $('#tmsselector').append('<option value="' + $(element).attr("href") + '">' + $(element).attr("title") + '</option>');
        });
        $('#tmsselector').change(redrawTMS);
    initMap();
    initMapLayers(map);

    ChangeProjection(map, vector_layerGeoJson);
    map.addLayer(vector_layerGeoJson);

    ChangeProjection(map, ori_layerGeoJson);
    map.addLayer(ori_layerGeoJson);

    ChangeProjection(map, inb_layerGeoJson);
    map.addLayer(inb_layerGeoJson);

    ChangeProjection(map, linbe_layerGeoJson);
    map.addLayer(linbe_layerGeoJson);
    
    redrawTMS();
});

function initMap() {
    map = new OpenLayers.Map('mapGeoJson', {
        numZoomLevels: 20,
        projection: epsg900913,
        displayProjection: epsg4326,
        theme:"theme/default/style-v2.css",
        allOverlays: true
    });
    initTMSLayer();
}

function initTMSLayer() {
    osmLayer = new OpenLayers.Layer.XYZ(
            "OpenStreetMap",
            [
                "http://otile1.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile2.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile3.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile4.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png"
            ],
            {
                transitionEffect: "resize",
                visibility: $('#CBosm').is(':checked'),
            }
        );
    map.addLayer(osmLayer);
    tmsLayer = new OpenLayers.Layer.TMS(
        "",
        [""],
        { 'type': 'png', 'getURL': get_my_url, sphericalMercator: false, isBaseLayer: false }
    );
    map.addLayer(tmsLayer);

    graticuleCtl = new OpenLayers.Control.Graticule({
        numPoints: 1,
        labelled: true,
        targetSize: 600,
        lineSymbolizer: { strokeColor: "#888888", strokeWidth: 1 },
        labelSymbolizer: { fontColor: "#000000", fontSize: "10px" }
    });
    map.addControl(graticuleCtl);
    graticuleCtl.activate();
    extent = new OpenLayers.Bounds(88, -17, 152, 17).transform(map.displayProjection, map.getProjectionObject());
    map.setCenter(new OpenLayers.LonLat(115.817, -0.842).transform(map.displayProjection, map.getProjectionObject()), 5);
    map.setOptions({ restrictedExtent: extent });
}

function toggleosm() {
	$('#btn_tools_osm').toggleClass('purple');
    osmLayer.visibility = $('#btn_tools_osm').hasClass('purple');
    osmLayer.redraw(true);
}

function redrawTMS() {
    tmsLayer.layername = $('#tmsselector :selected').html();
    tmsLayer.url = $('#tmsselector').val();
    tmsLayer.redraw(true);
}

function get_my_url(bounds) {
    var res = map.getResolution();
    var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
    var y = Math.round((90 - this.maxExtent.bottom + bounds.bottom) / (res * this.tileSize.h));
    var z = map.getZoom();

    var path = '/'+z+'/'+x+'/'+(y)+'.png?token=7d6b0e2c-3684-40de-8b8c-c50deea14231';
    var url = this.url;
    if (url instanceof Array) {
        url = this.selectUrl(path, url);
    }
    return url + path;

}

function getBounds() {
    return map.calculateBounds().transform(epsg900913, epsg4326);
}

function getCursorLonLat(mouseEvent) {
    return map.getLonLatFromViewPortPx(mouseEvent.xy).transform(epsg900913, epsg4326);
}