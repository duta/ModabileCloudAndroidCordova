
$(document).ready(function () {
    selectGeoJsonFeat = new OpenLayers.Control.SelectFeature([vector_layerGeoJson, inb_layerGeoJson, ori_layerGeoJson]); //this one should allow us to handle clicks
    map.addControl(selectGeoJsonFeat);
    selectGeoJsonFeat.activate();

    vector_layerGeoJson.events.on({
        "featureselected": function (e) {
            onFeatureGJSelect(e.feature);
            selectGeoJsonFeat.unselectAll();
        }
    });
    inb_layerGeoJson.events.on({
        "featureselected": function (e) {
            onFeatureGJSelect(e.feature);
            selectGeoJsonFeat.unselectAll();
        }
    });
    ori_layerGeoJson.events.on({
        "featureselected": function (e) {
            onFeatureGJSelect(e.feature);
            selectGeoJsonFeat.unselectAll();
        }
    });

    clickControl = new OpenLayers.Control.Click();
    map.addControl(clickControl);

    map.events.register("mousemove", map, MouseMove);
    // lon lat
    map.addControl(new OpenLayers.Control.MousePosition());
});

var centerM, circleRadFeat, clickControl, selectGeoJsonFeat, distance = 0, arrLine = [];
function MouseMove(e) {
    var lonlat = getCursorLonLat(e);
    if (prevClickTool) {
        if (centerM != undefined) {
            distance = OpenLayers.Util.distVincenty(lonlat, centerM);
            var boun = getBounds();
            var difX = (lonlat.lat - centerM.lat);
            var difY = (lonlat.lon - centerM.lon);
            var radius = Math.sqrt((difX * difX) + (difY * difY)) * map.size.h / (boun.top - boun.bottom);
            var style_blue = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
            style_blue.pointRadius = radius;
            style_blue.strokeWidth = 1;
            style_blue.strokeColor = "grey";
            style_blue.fillColor = "white";
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            circleRadFeat = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(centerM.lon, centerM.lat), null, style_blue);
            vector_layerGeoJson.addFeatures(circleRadFeat);
        }
    } else if (prevLineTool) {
        if (arrLine.length > 0) {
            var temp = arrLine.slice();
            temp.push(lonlat);
            var tempdraw = [];
            for (var i = 0; i < temp.length; i++) {
                tempdraw.push(new OpenLayers.Geometry.Point(temp[i].lon, temp[i].lat));
            }
            var line = new OpenLayers.Geometry.LineString(tempdraw);
            var style = { strokeColor: "grey", strokeWidth: 2 };
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            circleRadFeat = new OpenLayers.Feature.Vector(line, null, style);
            vector_layerGeoJson.addFeatures(circleRadFeat);
        }
    }
}

OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
    defaultHandlerOptions: {
        'single': true,
        'double': true,
        'pixelTolerance': 0,
        'stopSingle': false,
        'stopDouble': true
    },

    initialize: function (options) {
        this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
        OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    );
        this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click': this.trigger,
                            'dblclick': this.dbltrigger,
                        }, this.handlerOptions
                    );
    },
    trigger: MouseClick,
    dbltrigger: MouseDouble,
});

var prevClickTool = false, prevLineTool = false, prevWithLoa = false;

function MouseClick(e) {
    if (prevClickTool) {
        if (centerM != undefined) {
            centerM = undefined;
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            valueKm = Math.round(distance * 1000) / 1000;
            valueNauMile = valueKm * 0.54;
            alert("That circle's radius is about " + valueNauMile + " NMiles/" + valueKm + " KM");
        } else {
            centerM = getCursorLonLat(e);
        }
    } else {
        var lonlat = getCursorLonLat(e);
        arrLine.push(lonlat);
        if (arrLine.length > 1) distance += OpenLayers.Util.distVincenty(arrLine[arrLine.length - 2], arrLine[arrLine.length - 1]);
    }
}
function MouseDouble(e) {
    if (prevClickTool) {
        if (centerM != undefined) {
            centerM = undefined;
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            valueKm = Math.round(distance * 1000) / 1000;
            valueNauMile = valueKm * 0.54;
            alert("That circle's radius is about " + valueNauMile + " NMiles/" + valueKm + " KM");
        } else {
            centerM = getCursorLonLat(e);
        }
    } else {
        var lonlat = getCursorLonLat(e);
        arrLine.push(lonlat);
        if (arrLine.length > 1) distance += OpenLayers.Util.distVincenty(arrLine[arrLine.length - 2], arrLine[arrLine.length - 1]);
        vector_layerGeoJson.removeFeatures(circleRadFeat);
        valueKm = Math.round(distance * 1000) / 1000;
        valueNauMile = valueKm * 0.54;
        distance = 0;
        arrLine = [];
        alert("That line's length is about " + valueNauMile + " NMiles/" + valueKm + " KM");
    }
}

$(".toolsButton").click(function () {
    var id = $(this).attr('id');
    centerM = undefined;
    vector_layerGeoJson.removeFeatures(circleRadFeat);
    if (id == "tools_hand") {
        prevClickTool = false;
        prevLineTool = false;
        $("#btn_tools_hand").addClass("purple");
        $("#btn_tools_circle").removeClass("purple");
        $("#btn_tools_line").removeClass("purple");
    }
    if (id == "tools_circle") {
        prevClickTool = true;
        prevLineTool = false;
        $("#btn_tools_circle").addClass("purple");
        $("#btn_tools_hand").removeClass("purple");
        $("#btn_tools_line").removeClass("purple");
    }
    if (id == "tools_line") {
        prevClickTool = false;
        prevLineTool = true;
        $("#btn_tools_line").addClass("purple");
        $("#btn_tools_circle").removeClass("purple");
        $("#btn_tools_hand").removeClass("purple");
    }
    for (controlItem in map.controls) {
        map.controls[controlItem].deactivate();
    }
    if (prevClickTool || prevLineTool) {
        clickControl.activate();
    }
    else {
        selectGeoJsonFeat.activate();
        for (controlItem in map.controls) {
            map.controls[controlItem].activate();
        }
        clickControl.deactivate();
    }
});

function ToggleMenu() {
    $('.detailView').toggleClass('active');

}