package id.co.ptdmc.modabileandroid;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;

public class WebViewActivity extends Activity {

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Plus.API)
                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                    .build();
        }
        mGoogleApiClient.connect();
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_web_view);
        WebView mWebView = (WebView) findViewById(R.id.activity_main_webview);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        Bundle b = getIntent().getExtras();
        mWebView.addJavascriptInterface(new JavascriptBridge(mGoogleApiClient, this), "JSInterface");
        mWebView.loadUrl("file:///android_asset/www/index.html?token="+b.getString("token")+"&email="+b.getString("email"));
    }

    private void callMainAndSelfDestruct() {
        mGoogleApiClient.disconnect();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public class JavascriptBridge {

        private final GoogleApiClient mGoogleApiClient;
        private final WebViewActivity webViewActivity;

        public JavascriptBridge(GoogleApiClient mGoogleApiClient, WebViewActivity webViewActivity) {
            this.mGoogleApiClient = mGoogleApiClient;
            this.webViewActivity = webViewActivity;
        }

        @JavascriptInterface
        public String getToken() {
            String token = null;
            try {
                token = GoogleAuthUtil.getToken(
                        getApplicationContext(),
                        Plus.AccountApi.getAccountName(mGoogleApiClient),
                        "audience:server:client_id:820145933541-gvlb64b91uqtogs1jr7dluajqhj79gru.apps.googleusercontent.com");
                GoogleAuthUtil.clearToken(getApplicationContext(),token);
                token = GoogleAuthUtil.getToken(
                        getApplicationContext(),
                        Plus.AccountApi.getAccountName(mGoogleApiClient),
                        "audience:server:client_id:820145933541-gvlb64b91uqtogs1jr7dluajqhj79gru.apps.googleusercontent.com");
            } catch (Exception transientEx) {
                // Network or server error, try later
                Log.e("JavascriptBridge", transientEx.toString());
            }
            return token;
        }

        @JavascriptInterface
        public void signOut() {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            webViewActivity.callMainAndSelfDestruct();
        }
    }
}
